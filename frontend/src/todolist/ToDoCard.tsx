import {updateToDo, ToDo, deleteToDo} from "./ToDoService";
import {Checkbox, IconButton, ListItem, ListItemButton, ListItemIcon, Typography} from "@mui/material";
import {grey} from "@mui/material/colors";
import {DeleteOutline} from "@mui/icons-material";
import React, {useState} from "react";

type ToDoProps = { initialToDo: ToDo; updateToDos: () => void };
export const ToDoCard = ({initialToDo, updateToDos}: ToDoProps) => {

    const [toDo, setToDo] = useState<ToDo>(initialToDo);

    const handleClick = () => {
        updateToDo(toDo.id, toDo.status).then(toDoStatus => {
            setToDo((t) => ({
                ...t,
                id: t.id,
                status: t.status === 'complete' ? 'active' : 'complete'
            }))
            console.log(toDoStatus);
            }
        )}


    // const handleAdd = () => {
    //         updateToDo(toDo.status).then(toDoStatus => {
    //             setToDo((currentItems) => [...currentItems, toDoStatus]);
    //             setNewToDoText('');
    //         })
    //     }
    // }

    return (
        <ListItem
            sx={{mt: 1, borderRadius: 1, backgroundColor: grey[900], overflow: "hidden"}}
            secondaryAction={
                <IconButton
                    onClick={() => toDo.id && deleteToDo(toDo.id).then(updateToDos)}
                    color="error"
                    edge="end"
                    aria-label="delete-button"
                >
                    <DeleteOutline/>
                </IconButton>

            }
            disablePadding>
            <ListItemButton
                role={undefined}
                dense
                onClick={handleClick}>
                <ListItemIcon>
                    <Checkbox
                        edge="start"
                        checked={toDo.status === 'complete'}
                        tabIndex={-1}
                        disableRipple
                    />
                </ListItemIcon>
                <Typography fontSize="large">{toDo.text}</Typography>
            </ListItemButton>
        </ListItem>)
};